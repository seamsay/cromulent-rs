use cromulent::*;

#[test]
fn is_empty() {
    let words = WordExpander::default().expand("").unwrap();
    let words = utf8::WordList::from(&words);

    assert!(words.is_empty());
}

#[test]
fn not_is_empty() {
    let words = WordExpander::default().expand("something").unwrap();
    let words = utf8::WordList::from(&words);

    assert!(!words.is_empty());
}

#[test]
fn len_0() {
    let words = WordExpander::default().expand("").unwrap();
    let words = utf8::WordList::from(&words);

    assert_eq!(words.len(), 0);
}

#[test]
fn len_1() {
    let words = WordExpander::default().expand("something").unwrap();
    let words = utf8::WordList::from(&words);

    assert_eq!(words.len(), 1);
}

#[test]
fn len_2() {
    let words = WordExpander::default().expand("two things").unwrap();
    let words = utf8::WordList::from(&words);

    assert_eq!(words.len(), 2);
}

#[test]
fn index_1() {
    let words = WordExpander::default().expand("something").unwrap();
    let words = utf8::WordList::from(&words);
    let word = &words[0];

    assert_eq!(word, "something");
}

#[test]
fn index_2() {
    let words = WordExpander::default().expand("two things").unwrap();
    let words = utf8::WordList::from(&words);
    let word = &words[1];

    assert_eq!(word, "things");
}

#[test]
#[should_panic(expected = "index out of bounds")]
fn out_of_bounds_0() {
    let words = WordExpander::default().expand("").unwrap();
    let words = utf8::WordList::from(&words);

    let _ = &words[0];
}

#[test]
#[should_panic(expected = "index out of bounds")]
fn out_of_bounds_2() {
    let words = WordExpander::default().expand("two things").unwrap();
    let words = utf8::WordList::from(&words);

    let _ = &words[2];
}

#[test]
fn iteration() {
    let words = WordExpander::default().expand("1 2 3").unwrap();
    let words = utf8::WordList::from(&words);
    let words = words.into_iter().collect::<Vec<_>>();

    assert_eq!(words, vec!["1", "2", "3"]);
}
