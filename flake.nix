{
  inputs = {
    naersk.url = "github:nmattia/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
      in {
        defaultPackage = naersk-lib.buildPackage {
          root = ./.;

          LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";
        };

        devShell = with pkgs;
          mkShell {
            buildInputs =
              [ cargo rust-analyzer rustc rustfmt rustPackages.clippy ];

            # TODO: This is obviously wrong... `libclang` is used for `stddef.h` because the one in `linuxHeaders` doesn't include `size_t`.
            # TODO: Look to https://discourse.nixos.org/t/setting-up-a-nix-env-that-can-compile-c-libraries/15833/3
            C_INCLUDE_PATH = "${glibc.dev}/include:${llvmPackages.libclang.lib}/lib/clang/${llvmPackages.libclang.version}/include";
            LIBCLANG_PATH = "${llvmPackages.libclang.lib}/lib";
            RUST_SRC_PATH = rustPlatform.rustLibSrc;
          };
      });
}
