# `cromulent-rs`

[![Crate](https://img.shields.io/crates/v/cromulent)](https://crates.io/crates/cromulent)
[![Tests](https://img.shields.io/gitlab/pipeline-status/seamsay/cromulent-rs)](https://gitlab.com/seamsay/cromulent-rs/pipelines)
[![Coverage](https://gitlab.com/seamsay/Meander.jl/badges/main/coverage.svg)](https://gitlab.com/seamsay/Meander.jl/commits/main)
[![Docs](https://img.shields.io/docsrs/cromulent)](https://docs.rs/cromulent)

Expand [perfectly cromulent words](https://www.merriam-webster.com/words-at-play/what-does-cromulent-mean), with the `wordexp(3)` C function. See the [documentation](https://docs.rs/cromulent) for more information.

## Warning!

**_This crate has only been tested on Linux. It should work on all Unix flavours, but any other OS is probably going to be a crapshoot._**

## Future Work - Help Wanted!

### API Improvements

I don't believe for a second that the current API is ideal, far from in fact. If you have any ideas, please open an issue to discuss it.

### Portability

Linux works, other Unix OSs _probably_ work, anything else...  ¯\\\_(ツ)\_/¯

### Testing

Currently this crate is only tested on Linux, but it would be good to set up testing for macOS and a BSD.